﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP1.Métier;

namespace TP1.Métier
{
    class Intervenant
    {
        private string _nom;
        private int _tauxHoraire;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }
        
        public int TauxHoraire
        {
            get { return _tauxHoraire; }
            set { _tauxHoraire = value; }
        }

        public int getTauxHoraire(int _tauxHoraire)
        {
            return _tauxHoraire;
        }
        

    }
}
