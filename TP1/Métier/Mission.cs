﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP1.Métier;

namespace TP1.Métier
{
    class Mission
    {
        private string _nom;
        private string _description;
        private int _nbHeuresPrevues;
        private Dictionary<DateTime, int> _releveHoraire;
        private Intervenant _executant;
        

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }
        public string Description 
        {
            get { return _description; }
            set { _description = value; }
        }
        public int NbHeuresPrevues
        {
            get { return _nbHeuresPrevues; }
            set { _nbHeuresPrevues = value; }
        }
        public Dictionary<DateTime,int> getReleveHoraire(Dictionary<DateTime, int> _releveHoraire) 
        {
            return _releveHoraire;
        }
        public decimal nbHeuresEffectuees()
        {
            int heure = 0;
            foreach (KeyValuePair<DateTime,int> a in _releveHoraire)
            {
                heure += a.Value;
            }
            return heure;
        }

        public Intervenant getExecutant()
        {
            return _executant;
        }

        public void AjouteReleve(DateTime date,int nbHeures)
        {
            this._releveHoraire.Add(date, nbHeures);
        }
    }
}
