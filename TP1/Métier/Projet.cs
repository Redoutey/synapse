﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP1.Métier;

namespace TP1.Métier
{
    class Projet
    {
        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private int _prixFactureMO;
        private List<Mission> _mission;
        


        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public DateTime Debut
        {
            get { return _debut; }
            set { _debut = value; }
        }
        public int PrixFactureMo
        {
            get { return _prixFactureMO; }
            set { _prixFactureMO = value; }
        }
        private decimal CumulCourMo()
        {
            decimal cumul = 0;

            foreach (Mission m in _mission)
            {
                cumul += m.nbHeuresEffectuees() * m.getExecutant().getTauxHoraire();

            }

            return cumul;
        }
        public decimal MargeBruteCourante()
        {
            decimal marge = 0;
            marge = this.PrixFactureMo - this.CumulCourMo();
            return marge;
        }
    }

}
